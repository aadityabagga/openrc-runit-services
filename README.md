OpenRC services with supervision using runit.

You will need a corresponding runit service with the OpenRC service. See [runit guide](https://github.com/OpenRC/openrc/blob/master/runit-guide.md) for more information.

Several runit services are available [here](https://github.com/aadityabagga/runit-services/tree/update) and [here](https://gitlab.com/flussence/runit-scripts).

NOTE - Project in development, please test before deploying in production!
